package com.boss.androidkotlin

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_hw2_3.*

class HomeWork23Fragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_hw2_3,container,false)
        setListeners(view)
        return view
    }
    fun makeColored(view: View){
        when (view.id) {
            R.id.box_one_text -> view.setBackgroundColor(Color.DKGRAY)
            R.id.box_two_text -> view.setBackgroundColor(Color.GRAY)
            R.id.box_three_text -> view.setBackgroundColor(Color.BLUE)
            R.id.box_four_text -> view.setBackgroundColor(Color.MAGENTA)
            R.id.box_five_text -> view.setBackgroundColor(Color.BLUE)
            R.id.red_button -> box_three_text.setBackgroundResource(R.color.my_red)
            R.id.yellow_button -> box_four_text.setBackgroundResource(R.color.my_yellow)
            R.id.green_button -> box_five_text.setBackgroundResource(R.color.my_green)
            else -> view.setBackgroundColor(Color.LTGRAY)
        }
    }
    fun setListeners(view: View) {
        val boxOneText: TextView = view.findViewById(R.id.box_one_text)
        val boxTwoText: TextView = view.findViewById(R.id.box_two_text)
        val boxThreeText: TextView = view.findViewById(R.id.box_three_text)
        val boxFourText: TextView = view.findViewById(R.id.box_four_text)
        val boxFiveText: TextView = view.findViewById(R.id.box_five_text)
        val rootConstraintLayout: View = view.findViewById(R.id.constraint_layout)
        val redButton:TextView = view.findViewById(R.id.red_button)
        val greenButton:TextView = view.findViewById(R.id.green_button)
        val yellowButton:TextView = view.findViewById(R.id.yellow_button)
        val clickableViews: List<View> = listOf(boxOneText, boxTwoText, boxThreeText, boxFourText, boxFiveText, rootConstraintLayout, redButton, greenButton, yellowButton)
        for (item in clickableViews) {
            item.setOnClickListener { makeColored(it) }
        }
    }
}
