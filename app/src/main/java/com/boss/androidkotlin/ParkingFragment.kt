package com.boss.androidkotlin
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import com.boss.androidkotlin.databinding.FragmentParkingBinding
import java.lang.IndexOutOfBoundsException
import java.lang.NullPointerException

class ParkingFragment : Fragment() {
    private lateinit var binding: FragmentParkingBinding
    private val myParking: ArrayList<Parking> = ArrayList<Parking>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding  = DataBindingUtil.inflate(inflater ,R.layout.fragment_parking,container , false)
        var myView : View = binding.root
        var slot:Int = 0
        for (i in 0..2) {
            myParking.add(i,Parking("","",""))
        }
        binding.apply {
            binding.park1.setOnClickListener{
                slot=0
                setSlotData(slot)
                setVisible()
            }
            binding.park2.setOnClickListener{
                slot=1
                setSlotData(slot)
                setVisible()
            }
            binding.park3.setOnClickListener{
                slot=2
                setSlotData(slot)
                setVisible()
            }
            binding.updateBtn.setOnClickListener{
                updateSlot(slot)
            }
            binding.deleteBtn.setOnClickListener{
                deleteSlot(slot)
            }
            invalidateAll()
        }

        return myView
    }
    fun updateSlot(index:Int){
        var id:String = binding.carId.text.toString()
        var brand = binding.carBrand.text.toString()
        var name = binding.carAuthor.text.toString()
        myParking.removeAt(index)
        myParking.add(index,Parking(id,brand,name))
        try {
            if(index==0){
                binding.park1.setText(myParking.get(index).id)
            }else if(index==1){
                binding.park2.setText(myParking.get(index).id)
            }else if(index==2){
                binding.park3.setText(myParking.get(index).id)
            }
        } catch (e: IndexOutOfBoundsException) {

        }
        setVisibleGone()
        binding.carId.setText("")
        binding.carBrand.setText("")
        binding.carAuthor.setText("")
    }
    fun deleteSlot(index:Int){
        myParking.removeAt(index)
        myParking.add(index,Parking("","",""))
        if(index==0){
            binding.park1.setText("ว่าง")
        }else if(index==1){
            binding.park2.setText("ว่าง")
        }else if(index==2){
            binding.park3.setText("ว่าง")
        }
        binding.carId.visibility = View.GONE
        binding.carBrand.visibility = View.GONE
        binding.carAuthor.visibility = View.GONE
        binding.updateBtn.visibility = View.GONE
        binding.deleteBtn.visibility = View.GONE
    }
    fun setVisible(){
        binding.carId.visibility = View.VISIBLE
        binding.carBrand.visibility = View.VISIBLE
        binding.carAuthor.visibility = View.VISIBLE
        binding.updateBtn.visibility = View.VISIBLE
        binding.deleteBtn.visibility = View.VISIBLE
    }
    fun setVisibleGone(){
        binding.carId.setText("")
        binding.carBrand.setText("")
        binding.carAuthor.setText("")
        binding.carId.visibility = View.GONE
        binding.carBrand.visibility = View.GONE
        binding.carAuthor.visibility = View.GONE
        binding.updateBtn.visibility = View.GONE
        binding.deleteBtn.visibility = View.GONE
    }
    fun setSlotData(index:Int) {
        try {
            binding.carId.setText(myParking.get(index).id)
            binding.carBrand.setText(myParking.get(index).brand)
            binding.carAuthor.setText(myParking.get(index).name)
        } catch (e: IndexOutOfBoundsException) {
            binding.carId.setText("")
            binding.carBrand.setText("")
            binding.carAuthor.setText("")
        }
    }

}