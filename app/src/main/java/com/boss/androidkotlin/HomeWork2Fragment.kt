package com.boss.androidkotlin

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import com.boss.androidkotlin.databinding.FragmentHw2Binding
class HomeWork2Fragment : Fragment() {
    private lateinit var binding: FragmentHw2Binding
    private val myName: MyName = MyName("Aleks Haecky")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding  = DataBindingUtil.inflate(inflater ,R.layout.fragment_hw2,container , false)
        var myView : View  = binding.root
        binding.myName = myName
        binding.doneButton.setOnClickListener {
            binding.nicknameText.text = binding.nicknameEdit.text
            binding.nicknameEdit.visibility = View.GONE
            binding.doneButton.visibility = View.GONE
            binding.nicknameText.visibility = View.VISIBLE
            binding.nicknameText.text = binding.nicknameEdit.text.toString()
        }
        binding.apply {
            myName?.nickname = nicknameEdit.text.toString()
            invalidateAll()
        }
//        val view = inflater.inflate(R.layout.fragment_hw2,container,false)
//        view.findViewById<Button>(R.id.done_button).setOnClickListener{
//            val editText:EditText = view.findViewById(R.id.nickname_edit)
//            val nicknameTextView:TextView = view.findViewById(R.id.nickname_text)
//            nicknameTextView.text = editText.text
//            editText.visibility = View.GONE
//            nicknameTextView.visibility = View.VISIBLE
//            val doneButton:Button = view.findViewById(R.id.done_button)
//            doneButton.visibility = View.GONE
//            val context =this.getContext()
//            val inputMethodManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
//        }
//        view.findViewById<TextView>(R.id.nickname_text).setOnClickListener {
//            val editText:EditText = view.findViewById(R.id.nick
//            name_edit)
//            val doneButton:Button = view.findViewById(R.id.done_button)
//            editText.visibility = View.VISIBLE
//            doneButton.visibility = View.VISIBLE
//            view.findViewById<TextView>(R.id.nickname_text).visibility = View.GONE
//            editText.requestFocus()
//            val context =this.getContext()
//            val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//            imm.showSoftInput(editText, 0)
//        }
        return myView
    }
}