package com.boss.androidkotlin

import android.text.Editable

data class Parking(var id: String, var brand: String, var name: String)
